ATTENTION !!! This module will destroy your data in your drupal installation. You can select which entites, bundles, field types and fields you want to destroy and shredder will leave shreddered data for you.

Who needs that?

If you want to find a bug in a database of a customers installation and the database contains confidential data, you want to keep the data structure but anonymise all data. This is what Shredder will do. The structure will be kept but all data will be replaced by random values. After the shreddering process you can share databases without giving confidential data away,

Of course, before shreddering your data in a batch process, the modul will send you several warnings you have to confirm.

We use this module to debug ERPAL installations of our customers. Using the shredder module on a SEPARATED CLONED DATABASE, NOT YOUR LIVE DATABASE, we are able to share databases to get an easier debugging under live data environment.

This module is NOT recommended for live sites, only for development environments.